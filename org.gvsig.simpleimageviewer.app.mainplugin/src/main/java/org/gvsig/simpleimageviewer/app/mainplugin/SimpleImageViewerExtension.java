
package org.gvsig.simpleimageviewer.app.mainplugin;

import org.gvsig.andami.IconThemeHelper;
import org.gvsig.andami.plugins.Extension;
import org.gvsig.tools.util.ToolsUtilLocator;


public class SimpleImageViewerExtension extends Extension {

    @Override
    public void initialize() {
        String[] iconNames = new String[] {
            "imageviewer-rotate-left",
            "imageviewer-rotate-right",
            "imageviewer-zoom-in",
            "imageviewer-zoom-out",
            "imageviewer-zoom-1-1",
            "imageviewer-fit"
        };
        for (String iconName : iconNames) {
            IconThemeHelper.registerIcon(null, iconName, this);
        }
        ToolsUtilLocator.registerImageViewertManager(SimpleImageViewerManager.class);
    }

    @Override
    public void execute(String command) {

    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public boolean isVisible() {
        return false;
    }
    
}
