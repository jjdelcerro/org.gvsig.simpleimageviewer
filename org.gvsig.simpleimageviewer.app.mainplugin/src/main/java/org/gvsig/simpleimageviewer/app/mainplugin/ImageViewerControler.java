
package org.gvsig.simpleimageviewer.app.mainplugin;

import java.awt.BorderLayout;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.apache.commons.io.FilenameUtils;
import org.gvsig.imageviewer.ImageViewer;
import org.gvsig.tools.ToolsLocator;
import org.gvsig.tools.i18n.I18nManager;
import org.gvsig.tools.swing.api.ToolsSwingLocator;
import org.gvsig.tools.swing.api.ToolsSwingManager;
import org.gvsig.tools.swing.icontheme.IconTheme;
import org.gvsig.tools.swing.icontheme.IconThemeManager;
import org.jdesktop.swingx.JXImageView;


public class ImageViewerControler extends ImageViewerView implements ImageViewer {

    private static final int ROTATE_RIGHT = 0;
    private static final int ROTATE_LEFT = 1;
    
    private JXImageView imageView;
    
    public ImageViewerControler() {
        this.initComponents();
    }
    
    private void initComponents() {
        ToolsSwingManager toolsSwingManager = ToolsSwingLocator.getToolsSwingManager();
        
        this.imageView = new JXImageView();
        this.ImageContainer.setLayout(new BorderLayout());
        this.ImageContainer.add(this.imageView,BorderLayout.CENTER);
        
        toolsSwingManager.translate(this.btnZoomIn);
        toolsSwingManager.translate(this.btnZoomOut);
        toolsSwingManager.translate(this.btnRotateLeft);
        toolsSwingManager.translate(this.btnRotateRight);
        toolsSwingManager.translate(this.btnFit);
        toolsSwingManager.translate(this.btnZoom1_1);
        
        this.btnRotateLeft.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rotate(ROTATE_LEFT);
            }
        });
        
        this.btnRotateRight.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rotate(ROTATE_RIGHT);
            }
        });
        
        this.btnFit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                adjustScale();
            }
        });
        
        this.btnZoom1_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                imageView.setScale(1.0);
            }
        });
        
        this.btnZoomIn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                imageView.setScale(imageView.getScale()*2);
            }
        });
        
        this.btnZoomOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                imageView.setScale(imageView.getScale()*0.5);
            }
        });
        
        this.imageView.addPropertyChangeListener("scale", new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                onChangeScale();
            }
        });
    }

    @Override
    public ImageIcon loadImage(String imageName) {
        String iconName = FilenameUtils.getBaseName(imageName);
        IconThemeManager iconThemeManager = ToolsSwingLocator.getIconThemeManager();
        IconTheme theme = iconThemeManager.getCurrent();
        if( theme.exists(iconName) ) {
            return theme.get(iconName);
        }
        return super.loadImage(imageName); 
    }

    
    private void rotate(int mode) {
        double scale = imageView.getScale();
        Image img = imageView.getImage();
        BufferedImage src = new BufferedImage(
                    img.getWidth(null),
                    img.getHeight(null), 
                    BufferedImage.TYPE_INT_ARGB);
        BufferedImage dst = new BufferedImage(
                    img.getHeight(null), 
                    img.getWidth(null),
                    BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D)src.getGraphics();

        try {
            // smooth scaling
            g.drawImage(img, 0, 0, null);
        } finally {
            g.dispose();
        }

        AffineTransform trans;
        switch(mode) {
        case ROTATE_RIGHT:
            trans = AffineTransform.getRotateInstance(Math.PI/2,0,0);
            trans.translate(0,-src.getHeight());
            break;
        default:
        case ROTATE_LEFT:
            trans = AffineTransform.getRotateInstance(-Math.PI/2,0,0);
            trans.translate(-src.getWidth(),0);
            break;
        }
        BufferedImageOp op = new AffineTransformOp(trans, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        op.filter(src,dst);
        imageView.setImage(dst);            
        imageView.setScale(scale);
    }
    
    private void onChangeScale() {
        I18nManager i18n = ToolsLocator.getI18nManager();
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(3);
        nf.setMinimumFractionDigits(3);
        String scale = nf.format(this.imageView.getScale());
        this.lblMessages.setText(i18n.getTranslation("_Scale_{0}", new String[] {scale}));
    }
    
    private void adjustScale() {
        try {
            double sx;
            double sy;
            sx = (double)this.getWidth() / this.imageView.getImage().getWidth(null);
            sy = (double)this.getHeight() / this.imageView.getImage().getHeight(null);
            if( sx > sy ) {
                this.imageView.setScale(sy);
            } else {
                this.imageView.setScale(sx);
            }
        } catch(Exception ex) {
            // Ignore exception, don't crash
        }
    }
    
    @Override
    public void setImage(URL url) {
        try {
            this.imageView.setImage(url);
        } catch (IOException ex) {
            throw new IllegalArgumentException(ex);
        }
        onChangeScale();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                adjustScale();
            }
        });
    }

    @Override
    public void setImage(File file) {
        try {
            this.imageView.setImage(file);
        } catch (IOException ex) {
            throw new IllegalArgumentException(ex);
        }
        onChangeScale();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                adjustScale();
            }
        });
    }

    @Override
    public JComponent asJComponent() {
        return this;
    }
}
