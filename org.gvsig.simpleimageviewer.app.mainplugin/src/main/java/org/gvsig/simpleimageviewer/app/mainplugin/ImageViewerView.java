package org.gvsig.simpleimageviewer.app.mainplugin;

import com.jeta.open.i18n.I18NUtils;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class ImageViewerView extends JPanel
{
   JPanel ImageContainer = new JPanel();
   JButton btnRotateLeft = new JButton();
   JButton btnRotateRight = new JButton();
   JButton btnZoomIn = new JButton();
   JButton btnZoomOut = new JButton();
   JButton btnZoom1_1 = new JButton();
   JButton btnFit = new JButton();
   JLabel lblMessages = new JLabel();

   /**
    * Default constructor
    */
   public ImageViewerView()
   {
      initializePanel();
   }

   /**
    * Adds fill components to empty cells in the first row and first column of the grid.
    * This ensures that the grid spacing will be the same as shown in the designer.
    * @param cols an array of column indices in the first row where fill components should be added.
    * @param rows an array of row indices in the first column where fill components should be added.
    */
   void addFillComponents( Container panel, int[] cols, int[] rows )
   {
      Dimension filler = new Dimension(10,10);

      boolean filled_cell_11 = false;
      CellConstraints cc = new CellConstraints();
      if ( cols.length > 0 && rows.length > 0 )
      {
         if ( cols[0] == 1 && rows[0] == 1 )
         {
            /** add a rigid area  */
            panel.add( Box.createRigidArea( filler ), cc.xy(1,1) );
            filled_cell_11 = true;
         }
      }

      for( int index = 0; index < cols.length; index++ )
      {
         if ( cols[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(cols[index],1) );
      }

      for( int index = 0; index < rows.length; index++ )
      {
         if ( rows[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(1,rows[index]) );
      }

   }

   /**
    * Helper method to load an image file from the CLASSPATH
    * @param imageName the package and name of the file to load relative to the CLASSPATH
    * @return an ImageIcon instance with the specified image file
    * @throws IllegalArgumentException if the image resource cannot be loaded.
    */
   public ImageIcon loadImage( String imageName )
   {
      try
      {
         ClassLoader classloader = getClass().getClassLoader();
         java.net.URL url = classloader.getResource( imageName );
         if ( url != null )
         {
            ImageIcon icon = new ImageIcon( url );
            return icon;
         }
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
      throw new IllegalArgumentException( "Unable to load image: " + imageName );
   }

   /**
    * Method for recalculating the component orientation for 
    * right-to-left Locales.
    * @param orientation the component orientation to be applied
    */
   public void applyComponentOrientation( ComponentOrientation orientation )
   {
      // Not yet implemented...
      // I18NUtils.applyComponentOrientation(this, orientation);
      super.applyComponentOrientation(orientation);
   }

   public JPanel createPanel()
   {
      JPanel jpanel1 = new JPanel();
      FormLayout formlayout1 = new FormLayout("FILL:4DLU:NONE,FILL:DEFAULT:GROW(1.0),FILL:4DLU:NONE","CENTER:2DLU:NONE,CENTER:DEFAULT:NONE,CENTER:2DLU:NONE,FILL:DEFAULT:GROW(1.0),CENTER:2DLU:NONE,CENTER:DEFAULT:NONE,CENTER:2DLU:NONE");
      CellConstraints cc = new CellConstraints();
      jpanel1.setLayout(formlayout1);

      ImageContainer.setName("ImageContainer");
      jpanel1.add(ImageContainer,cc.xy(2,4));

      jpanel1.add(createPanel1(),cc.xy(2,2));
      lblMessages.setName("lblMessages");
      jpanel1.add(lblMessages,cc.xy(2,6));

      addFillComponents(jpanel1,new int[]{ 1,2,3 },new int[]{ 1,2,3,4,5,6,7 });
      return jpanel1;
   }

   public JPanel createPanel1()
   {
      JPanel jpanel1 = new JPanel();
      FormLayout formlayout1 = new FormLayout("FILL:DEFAULT:NONE,FILL:4DLU:NONE,FILL:DEFAULT:NONE,FILL:4DLU:NONE,FILL:DEFAULT:NONE,FILL:4DLU:NONE,FILL:DEFAULT:NONE,FILL:4DLU:NONE,FILL:DEFAULT:NONE,FILL:4DLU:NONE,FILL:DEFAULT:NONE,FILL:DEFAULT:GROW(1.0)","CENTER:DEFAULT:NONE");
      CellConstraints cc = new CellConstraints();
      jpanel1.setLayout(formlayout1);

      btnRotateLeft.setIcon(loadImage("images/imageviewer-rotate-left.png"));
      btnRotateLeft.setName("btnRotateLeft");
      btnRotateLeft.setToolTipText("_Rotate_left");
      EmptyBorder emptyborder1 = new EmptyBorder(2,2,2,2);
      btnRotateLeft.setBorder(emptyborder1);
      jpanel1.add(btnRotateLeft,cc.xy(1,1));

      btnRotateRight.setIcon(loadImage("images/imageviewer-rotate-right.png"));
      btnRotateRight.setName("btnRotateRight");
      btnRotateRight.setToolTipText("_Rotate_right");
      EmptyBorder emptyborder2 = new EmptyBorder(2,2,2,2);
      btnRotateRight.setBorder(emptyborder2);
      jpanel1.add(btnRotateRight,cc.xy(3,1));

      btnZoomIn.setIcon(loadImage("images/imageviewer-zoom-in.png"));
      btnZoomIn.setName("btnZoomIn");
      btnZoomIn.setToolTipText("_Zoom_in");
      EmptyBorder emptyborder3 = new EmptyBorder(2,2,2,2);
      btnZoomIn.setBorder(emptyborder3);
      jpanel1.add(btnZoomIn,cc.xy(9,1));

      btnZoomOut.setIcon(loadImage("images/imageviewer-zoom-out.png"));
      btnZoomOut.setName("btnZoomOut");
      btnZoomOut.setToolTipText("_Zoom_out");
      EmptyBorder emptyborder4 = new EmptyBorder(2,2,2,2);
      btnZoomOut.setBorder(emptyborder4);
      jpanel1.add(btnZoomOut,cc.xy(11,1));

      btnZoom1_1.setIcon(loadImage("images/imageviewer-zoom-1-1.png"));
      btnZoom1_1.setName("btnZoom1_1");
      btnZoom1_1.setToolTipText("_Zoom_1_1");
      EmptyBorder emptyborder5 = new EmptyBorder(2,2,2,2);
      btnZoom1_1.setBorder(emptyborder5);
      jpanel1.add(btnZoom1_1,cc.xy(5,1));

      btnFit.setIcon(loadImage("images/imageviewer-fit.png"));
      btnFit.setName("btnFit");
      btnFit.setToolTipText("_Fit_to_window");
      EmptyBorder emptyborder6 = new EmptyBorder(2,2,2,2);
      btnFit.setBorder(emptyborder6);
      jpanel1.add(btnFit,cc.xy(7,1));

      addFillComponents(jpanel1,new int[]{ 2,4,6,8,10,12 },new int[0]);
      return jpanel1;
   }

   /**
    * Initializer
    */
   protected void initializePanel()
   {
      setLayout(new BorderLayout());
      add(createPanel(), BorderLayout.CENTER);
   }


}
