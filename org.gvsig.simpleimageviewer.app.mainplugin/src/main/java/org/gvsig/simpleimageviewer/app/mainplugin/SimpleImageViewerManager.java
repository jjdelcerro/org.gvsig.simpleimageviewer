
package org.gvsig.simpleimageviewer.app.mainplugin;

import org.gvsig.imageviewer.ImageViewer;
import org.gvsig.imageviewer.ImageViewerManager;


public class SimpleImageViewerManager implements ImageViewerManager {
    
    @Override
    public ImageViewer createImageViewer() {
        return new ImageViewerControler();
    }
    
}
